# Trabajo Practico de Sistemas Distribuidos

## Requerimientos 
- NodeJS
- JDK
- Maven

## Uso

### Servidor
1. Instalar las dependencias de Maven
2. Ejecutar `telefonia/src/main/java/puntoytedigo/App.java`

### Cliente

1. Ejecutar `node client/index.js`
