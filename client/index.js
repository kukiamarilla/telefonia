var express = require('express');
var udp = require('dgram');
var buffer = require('buffer');

var app = express();
var net = require('net');
var client = new net.Socket();

app.get('/', function (req, res) {
    client.write("Hola");
})

var server = app.listen(8081, function () {
    var host = server.address().address
    var port = server.address().port

    console.log("Example app listening at http://%s:%s", host, port)
})
//Envía mensaje por TCP
client.connect(5556, '127.0.0.1', function () {
    console.log('Connected');
    client.write('Hola');
});
client.on('data', function (data) {
    console.log('Received: ' + data);
    client.destroy(); // kill client after server's response
});

client.on('close', function () {
    console.log('Connection closed');
});


