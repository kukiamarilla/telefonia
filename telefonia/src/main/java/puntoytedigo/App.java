package puntoytedigo;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import puntoytedigo.model.Client;
import puntoytedigo.server.ConnectionHandler;
/**
 * Hello world!
 *
 */
public class App 
{
    private static final int PORT = 5556;
    public static ArrayList<Client> clientes;
    public static int idCounter = 0;
    public static void main( String[] args )
    {
        try(ServerSocket serverSocket = new ServerSocket(PORT)) {
            System.out.println("Servidor Levantado");
            while(true) {
                Socket cliente = serverSocket.accept();
                cliente.setKeepAlive(true);
                new ConnectionHandler(cliente).start();
            }
        } catch(IOException e) {
            System.out.println("No se pudo iniciar le servidor");
        }
    }
}
