package puntoytedigo.model;

import java.util.LinkedList;
import java.util.Objects;
import java.util.Queue;

import javax.persistence.Entity;

@Entity
public class Client {
    private int id;
    private String name;
    private Queue<Message> message;
    private boolean inCall;
    private boolean connected;


    public Client() {
        this.message = new LinkedList<Message>();
    }
    

    public Client(int id, String name, Queue<Message> message, boolean inCall) {
        this.id = id;
        this.name = name;
        this.message = new LinkedList<Message>();
        this.inCall = false;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Message pollMessage() {
        return this.message.poll();
    }

    public void pushMessage(Message message) {
        this.message.add(message);
    }
    public boolean hasMessage() {
        return !this.message.isEmpty();
    }

    public boolean isInCall() {
        return this.inCall;
    }

    public boolean getInCall() {
        return this.inCall;
    }

    public void setInCall(boolean inCall) {
        this.inCall = inCall;
    }
    
    public boolean getConnected() {
        return this.connected;
    }

    public void setConnected(boolean connected) {
        this.connected = true;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Client)) {
            return false;
        }
        Client client = (Client) o;
        return id == client.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public String toJson() {
        return "{" +
            "\"id\": " + getId() + "," +
            "\"name\": \"" + getName() + "\"" +
            "}";
    }





}