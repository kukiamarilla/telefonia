package puntoytedigo.server;

import puntoytedigo.model.Client;

public interface ControllerAdapter {
    void execute(String payload, Client cliente);
}