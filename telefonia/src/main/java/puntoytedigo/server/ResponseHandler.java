package puntoytedigo.server;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

import puntoytedigo.model.Client;
import puntoytedigo.model.Message;

public class ResponseHandler extends Thread {
    private Socket socket;
    private Client cliente;

    public ResponseHandler(Socket socket, Client cliente) {
        this.socket = socket;
        this.cliente = cliente;
    }

    public void run() {

        while (cliente.getConnected()) {
            if (cliente.hasMessage()) {
                Message message = cliente.pollMessage();
                try {
                    OutputStream out = this.socket.getOutputStream();
                    out.write(message.getMensaje().getBytes());
                    out.close();
                } catch (IOException e) {
                    System.out.println("Fallo el envío del mensaje: "+ message.getMensaje());
                    e.printStackTrace();
                }

            }
        }
    }
}