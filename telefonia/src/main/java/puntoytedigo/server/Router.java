package puntoytedigo.server;

import java.util.HashMap;

import puntoytedigo.model.Client;

public class Router {
    private HashMap<String,ControllerAdapter> routes;
    private Client cliente;
    public Router(Client cliente) {
        this.cliente = cliente;
        this.routes.put("clientes", new ControllerAdapter(){
            @Override
            public void execute(String payload, Client cliente) {
                Controller.getClients(payload, cliente);
            }
        });
    }
    public void execute(String route) {
        ControllerAdapter adapter = this.routes.get(route);
        adapter.execute(null, this.cliente);
    }
    public void execute(String route, String payload) {
        ControllerAdapter adapter = this.routes.get(route);
        adapter.execute(payload, this.cliente);
    }
}