package puntoytedigo.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import puntoytedigo.model.*;
import puntoytedigo.App;

public class ConnectionHandler extends Thread {

    private Socket socket;
    private Client cliente;

    public ConnectionHandler(Socket socket) {
        this.socket = socket;
        cliente = new Client();
        cliente.setId(App.idCounter);
        cliente.setConnected(true);
        App.idCounter++;
    }

    public void run() {
        BufferedReader in;
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            String request;
            System.out.println("Nueva Conexion");
            while((request = in.readLine()) != null) {
                System.out.println("Mensaje: ");
                System.out.println(request);
            }
            System.out.println("Cerrado");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}